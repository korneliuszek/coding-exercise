package gamesys.registration;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import gamesys.registration.exclusion.ExclusionService;
import gamesys.registration.exclusion.UserExcludedException;
import gamesys.registration.user.User;
import gamesys.registration.user.UserDao;
import gamesys.registration.user.UserDaoImpl;
import gamesys.registration.user.UserExistsException;

public class RegistrationServiceTest {

	@Mock
	ExclusionService es;
	@Mock
	UserDao ud;

	@InjectMocks
	RegistrationServiceImpl rs;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldRegister() throws UserExistsException, UserExcludedException {
		when(es.validate(any(String.class), any(String.class))).thenReturn(true);
		when(ud.getUser("123")).thenReturn(null);
		User user = new User("123", "123", "abc", "abcd");
		rs.registerUser(user);
		verify(ud).createUser(user);
	}

	@Test(expected = UserExistsException.class)
	public void shouldNotRegisterExistingUser() throws UserExistsException, UserExcludedException {
		when(es.validate(any(String.class), any(String.class))).thenReturn(true);
		User user = new User("123", "123", "abc", "abcd");
		when(ud.getUser("123")).thenReturn(user);
		rs.registerUser(user);
	}

	@Test(expected = UserExcludedException.class)
	public void shouldNotRegisterExcludedUser() throws UserExistsException, UserExcludedException {
		User user = new User("123", "123", "abc", "abcd");
		when(es.validate(user.getDateOfBirth(), user.getSsn())).thenReturn(false);
		rs.registerUser(user);
	}
}
