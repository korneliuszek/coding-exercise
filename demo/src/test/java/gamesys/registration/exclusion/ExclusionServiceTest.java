package gamesys.registration.exclusion;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import gamesys.registration.exclusion.DummyExclusionService;
import gamesys.registration.exclusion.ExclusionService;

public class ExclusionServiceTest {

	private static final String EXCLUDED_SSN = "123456789";
	private static final String CORRECT_SSN = "12345678";
	private static final String SAMPLE_DOB = "123";
	private ExclusionService exclusionService;
	
	@Before
	public void setUp() {
		exclusionService = new DummyExclusionService();
	}
	
	@Test
	public void shouldValidate() {
		assertTrue(exclusionService.validate(SAMPLE_DOB, CORRECT_SSN));
	}
	
	@Test
	public void shouldNotValidate() {
		assertFalse(exclusionService.validate(SAMPLE_DOB, EXCLUDED_SSN));
	}
}
