package gamesys.registration.exclusion;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import gamesys.registration.exclusion.UserExcludedException;

public class UserExcludedExceptionTest {
	private static final String SAMPLE_MESSAGE = "User with ssn:123 is excluded.";

	@Test
	public void shouldCreteAndGetMessage() {
		UserExcludedException uee = new UserExcludedException(SAMPLE_MESSAGE);
		assertEquals(SAMPLE_MESSAGE, uee.getMessage());
	}
	
}
