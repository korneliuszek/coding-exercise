package gamesys.registration;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

import gamesys.registration.exclusion.UserExcludedException;
import gamesys.registration.user.User;
import gamesys.registration.user.UserExistsException;

public class RegistrationControllerTest {

	private static final String NAME_JSON_PATH = "$.name";
	private static final String PASSWORD_JSON_PATH = "$.password";
	private static final String DATE_OF_BIRTH_JSON_PATH = "$.dateOfBirth";
	private static final String SSN_JSON_PATH = "$.ssn";
	private static final String NAME_TO_SHORT_MESSAGE = "Name to short.";
	private static final String PASSWORD_TO_SHORT_MESSAGE = "Password to short.";
	private static final String INVALID_SSN_MESSAGE = "Invalid ssn.";
	private static final String INVALID_DATE_MESSAGE = "Invalid date.";
	private static final String INVALID_DATE = "1988-121-12";
	private static final String INVALID_SSN = "abc";
	private static final String TO_SHORT_NAME = "a";
	private static final String TO_SHORT_PASSWORD = "abcd";
	private static final String USER_EXISTS_MESSAGE = "User exists.";
	private static final String USER_EXCLUDED_MESSAGE = "User excluded.";
	private static final String SAMPLE_NAME = "ab";
	private static final String SAMPLE_PASSWORD = "abcde";
	private static final String SAMPLE_SSN = "1234";
	private static final String REGISTER_USER_PATH = "/registerUser";
	private static final String SAMPLE_DATE = "1988-12-12";
	@Mock
	RegistrationService rs;
	@InjectMocks
	RegistrationController rc;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldRegister() throws Exception {
		User created = new User(SAMPLE_SSN, SAMPLE_DATE, SAMPLE_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenReturn(created);
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isOk()).andExpect(jsonPath(SSN_JSON_PATH, is(created.getSsn())))
				.andExpect(jsonPath(DATE_OF_BIRTH_JSON_PATH, is(created.getDateOfBirth())))
				.andExpect(jsonPath(PASSWORD_JSON_PATH, is(created.getPassword())))
				.andExpect(jsonPath(NAME_JSON_PATH, is(created.getName())));
	}
	
	@Test
	public void shouldNotValidateShortPassword() throws Exception {
		User created = new User(SAMPLE_SSN, SAMPLE_DATE, TO_SHORT_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenReturn(created);
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isBadRequest()).andExpect(content().string(PASSWORD_TO_SHORT_MESSAGE));
	}
	
	@Test
	public void shouldNotValidateShortName() throws Exception {
		User created = new User(SAMPLE_SSN, SAMPLE_DATE, SAMPLE_PASSWORD, TO_SHORT_NAME);
		when(rs.registerUser(created)).thenReturn(created);
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isBadRequest()).andExpect(content().string(NAME_TO_SHORT_MESSAGE));
	}
	
	@Test
	public void shouldNotValidateInvalidSsn() throws Exception {
		User created = new User(INVALID_SSN, SAMPLE_DATE, SAMPLE_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenReturn(created);
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isBadRequest()).andExpect(content().string(INVALID_SSN_MESSAGE));
	}
	
	@Test
	public void shouldNotValidateInvalidDate() throws Exception {
		User created = new User(SAMPLE_SSN, INVALID_DATE, SAMPLE_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenReturn(created);
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isBadRequest()).andExpect(content().string(INVALID_DATE_MESSAGE));
	}
	
	@Test
	public void shouldNotRegisterExcluded() throws Exception {
		User created = new User(SAMPLE_SSN, SAMPLE_DATE, SAMPLE_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenThrow(new UserExcludedException(USER_EXCLUDED_MESSAGE));
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isForbidden()).andExpect(content().string(USER_EXCLUDED_MESSAGE));
	}
	
	@Test
	public void shouldNotRegisterDuplicate() throws Exception {
		User created = new User(SAMPLE_SSN, SAMPLE_DATE, SAMPLE_PASSWORD, SAMPLE_NAME);
		when(rs.registerUser(created)).thenThrow(new UserExistsException(USER_EXISTS_MESSAGE));
		MockMvc rcMock = standaloneSetup(rc).build();
		Gson gson = new Gson();
		String userJS = gson.toJson(created);
		rcMock.perform(post(REGISTER_USER_PATH).contentType(MediaType.APPLICATION_JSON).content(userJS))
				.andExpect(status().isConflict()).andExpect(content().string(USER_EXISTS_MESSAGE));
	}
}
