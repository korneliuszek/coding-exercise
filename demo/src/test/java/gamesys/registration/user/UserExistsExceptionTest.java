package gamesys.registration.user;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UserExistsExceptionTest {
	
	private static final String SAMPLE_MESSAGE = "User with ssn:123 already exists.";

	@Test
	public void shouldCreteAndGetMessage() {
		UserExistsException uee = new UserExistsException(SAMPLE_MESSAGE);
		assertEquals(SAMPLE_MESSAGE, uee.getMessage());
	}
}
