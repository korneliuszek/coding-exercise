package gamesys.registration.user;

import static org.junit.Assert.*;

import org.junit.Test;

public class UserTest {

	private static final String NAME = "name";
	private static final String PASSWORD = "pass";
	private static final String DATE_OF_BIRTH = "1988-01-01";
	private static final String SSN = "123-456";

	@Test
	public void shouldConstructAndGet() {
		User user = new User(SSN, DATE_OF_BIRTH, PASSWORD, NAME);
		assertEquals(SSN, user.getSsn());
		assertEquals(DATE_OF_BIRTH, user.getDateOfBirth());
		assertEquals(PASSWORD, user.getPassword());
		assertEquals(NAME, user.getName());
	}
	
	@Test
	public void shouldSetAndGet() {
		User user = new User();
		user.setSsn(SSN);
		user.setDateOfBirth(DATE_OF_BIRTH);
		user.setPassword(PASSWORD);
		user.setName(NAME);
		assertEquals(SSN, user.getSsn());
		assertEquals(DATE_OF_BIRTH, user.getDateOfBirth());
		assertEquals(PASSWORD, user.getPassword());
		assertEquals(NAME, user.getName());
	}
}
