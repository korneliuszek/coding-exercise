package gamesys.registration.user;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

import org.junit.Test;

public class UserDaoTest {
	
	private static final String SAMPLE_SSN = "123";

	@Test
	public void shouldAddAndGet() {
		UserDao userDao = new UserDaoImpl();
		User toAdd = new User(SAMPLE_SSN,"1234","abc","abcd");
		userDao.createUser(toAdd);
		User retrieved = userDao.getUser(SAMPLE_SSN);
		assertNotNull(retrieved);
		assertSame(toAdd, retrieved);
	}
}
