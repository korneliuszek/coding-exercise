package gamesys.registration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import gamesys.registration.exclusion.ExclusionService;
import gamesys.registration.exclusion.UserExcludedException;
import gamesys.registration.user.User;
import gamesys.registration.user.UserDao;
import gamesys.registration.user.UserExistsException;

@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	ExclusionService exclusionService;
	@Autowired
	UserDao registrationDao;
	
	@Override
	public User registerUser(User user) throws UserExistsException, UserExcludedException {
		if(registrationDao.getUser(user.getSsn()) != null) {
			throw new UserExistsException("User with ssn: " + user.getSsn() + " already exists.");
		}
		if(!exclusionService.validate(user.getDateOfBirth(), user.getSsn())) {
			throw new UserExcludedException("User with ssn: " + user.getSsn() + " is excluded.");
		}
		return registrationDao.createUser(user);
	}

}
