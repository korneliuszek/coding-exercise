package gamesys.registration.user;

public interface UserDao {

	User createUser(User user);
	User getUser(String ssn);
}