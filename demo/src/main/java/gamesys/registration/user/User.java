package gamesys.registration.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

public class User {
	@NotEmpty(message = "Ssn can't be empty.")
	@Pattern(regexp = "[\\d -]{4,12}", message = "Invalid ssn.") // Made up pattern for ssn
	private String ssn;
	@NotEmpty(message = "Date of birth can't be empty.")
	@Pattern(regexp = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$", message = "Invalid date.")
	private String dateOfBirth;
	@NotNull(message = "Password can't be empty.")
	@Size(min = 5, message = "Password to short.")
	private String password;
	@NotNull(message = "Name can't be empty.")
	@Size(min = 2, message = "Name to short.")
	private String name;

	public User() {
		super();
	}

	public User(String ssn, String dateOfBirth, String password, String name) {
		super();
		this.ssn = ssn;
		this.dateOfBirth = dateOfBirth;
		this.password = password;
		this.name = name;
	}

	public String getSsn() {
		return ssn;
	}

	public void setSsn(String ssn) {
		this.ssn = ssn;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "User [ssn=" + ssn + ", dateOfBirth=" + dateOfBirth + ", password=" + password + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}

}