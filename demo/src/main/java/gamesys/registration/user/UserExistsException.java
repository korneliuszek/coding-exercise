package gamesys.registration.user;

public class UserExistsException extends Exception {

	private static final long serialVersionUID = 3117698726131510745L;

	public UserExistsException(String message) {
		super(message);
	}

}
