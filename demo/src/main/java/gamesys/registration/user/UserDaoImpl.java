package gamesys.registration.user;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gamesys.registration.exclusion.ExclusionService;

@Component
public class UserDaoImpl implements UserDao {
	
	private Map<String, User> users = new HashMap<>();
	
	@Override
	public User createUser(User user) {
		if(!users.containsKey(user.getSsn())) {
			System.out.println("Adding user: " + user);
			users.put(user.getSsn(), user);
		}
		System.out.println("Current users repo: ");
		for(User u : users.values()) {
			System.out.println(u);
		}
		return user;
	}

	@Override
	public User getUser(String ssn) {
		System.out.println("User exists.");
		return users.get(ssn);
	}
	
	
	
}
