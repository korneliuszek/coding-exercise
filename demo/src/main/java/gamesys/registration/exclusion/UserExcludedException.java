package gamesys.registration.exclusion;

public class UserExcludedException extends Exception {

	private static final long serialVersionUID = -691477253632723250L;

	public UserExcludedException(String message) {
		super(message);
	}

}
