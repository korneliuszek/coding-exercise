package gamesys.registration.exclusion;

import org.springframework.stereotype.Component;

@Component
public class DummyExclusionService implements ExclusionService {

	@Override
	public boolean validate(String dob, String ssn) {
		if(ssn.equals("123456789")) {
			return false;
		}
		return true;
	}

}
