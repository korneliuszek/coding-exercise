package gamesys.registration;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import gamesys.registration.exclusion.UserExcludedException;
import gamesys.registration.user.User;
import gamesys.registration.user.UserExistsException;

@RestController
public class RegistrationController {

	@Autowired
	RegistrationService registrationService;
	
	@RequestMapping(value="/registerUser", method=RequestMethod.POST, consumes="application/json")
	public User registerUser(@Valid @RequestBody User user) throws UserExistsException, UserExcludedException {
		return registrationService.registerUser(user);
	}
	
	@ExceptionHandler(UserExistsException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public String userExists(UserExistsException e) {
		return e.getMessage();
	}
	
	@ExceptionHandler(UserExcludedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public String userExcluded(UserExcludedException e) {
		return e.getMessage();
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String validationError(MethodArgumentNotValidException e) {
		return e.getBindingResult().getFieldError().getDefaultMessage();
	}
}
