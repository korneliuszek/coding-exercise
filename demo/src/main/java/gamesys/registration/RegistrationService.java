package gamesys.registration;

import gamesys.registration.exclusion.UserExcludedException;
import gamesys.registration.user.User;
import gamesys.registration.user.UserExistsException;

public interface RegistrationService {
	public User registerUser(User user) throws UserExistsException, UserExcludedException;
}
